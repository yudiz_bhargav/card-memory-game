import React from 'react';

const cardBack = ({ singleSvg }) => {
  return (
    <figure className='back'>
      <img className='back-img' src={singleSvg[`svg${singleSvg.id}`]} alt='' />
    </figure>
  );
};

export default cardBack;

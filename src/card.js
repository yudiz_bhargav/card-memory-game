import React, { useState, useEffect, useRef } from 'react';
import CardFront from './cardFront';
import CardBack from './cardBack';
import svgData from './svgDataArray';

// MAIN COMPONENT
const Card = () => {
  const renderCount = useRef(0);

  const [currentCardKey, setCurrentCardKey] = useState(0);

  const [firstCardSvg, setFirstCardSvg] = useState('');
  const [secondCardSvg, setSecondCardSvg] = useState('');

  const countForNumberOfRightChoices = useRef(0);

  useEffect(() => {
    renderCount.current = renderCount.current + 1;

    if (renderCount.current === 3) {
      if (firstCardSvg === secondCardSvg) {
        countForNumberOfRightChoices.current += 1;

        const cardOne = document.querySelector('.card-one');
        const cardTwo = document.querySelector('.card-two');

        cardOne.classList.remove('card-one');
        cardTwo.classList.remove('card-two');
        setFirstCardSvg('');
        setSecondCardSvg('');
        renderCount.current = 0;
      } else {
        const cardOne = document.querySelector('.card-one');
        const cardTwo = document.querySelector('.card-two');

        setTimeout(() => {
          cardOne.classList.remove('card-is-flipped');
          cardOne.classList.remove('card-one');
          cardTwo.classList.remove('card-is-flipped');
          cardTwo.classList.remove('card-two');
          setFirstCardSvg('');
          setSecondCardSvg('');
          renderCount.current = 0;
        }, 1000);
      }
    }
  }, [firstCardSvg, secondCardSvg]);

  const clickHandler = (e) => {
    if (
      e.target.getAttribute('class') !== 'back' &&
      e.target.getAttribute('class') !== 'back-img'
    ) {
      if (renderCount.current === 1) {
        if (e.target.parentElement.tagName === 'DIV') {
          e.target.parentElement.classList.add('card-is-flipped');

          e.target.parentElement.classList.add('card-one');

          const imgTag = e.target.parentElement.querySelector('.back-img');
          const srcAttribute = imgTag.getAttribute('src');
          setFirstCardSvg(srcAttribute);
          const keyAttribute = e.target.parentElement.getAttribute('data-id');
          setCurrentCardKey(keyAttribute);
        } else if (e.target.parentElement.tagName === 'FIGURE') {
          e.target.parentElement.parentElement.classList.add('card-is-flipped');

          e.target.parentElement.parentElement.classList.add('card-one');

          const imgTag =
            e.target.parentElement.parentElement.querySelector('.back-img');
          const srcAttribute = imgTag.getAttribute('src');
          setFirstCardSvg(srcAttribute);

          const keyAttribute =
            e.target.parentElement.parentElement.getAttribute('data-id');
          setCurrentCardKey(keyAttribute);
        } else if (e.target.parentElement.tagName === 'svg') {
          e.target.parentElement.parentElement.parentElement.classList.add(
            'card-is-flipped'
          );
          e.target.parentElement.parentElement.parentElement.classList.add(
            'card-one'
          );

          const imgTag =
            e.target.parentElement.parentElement.parentElement.querySelector(
              '.back-img'
            );
          const srcAttribute = imgTag.getAttribute('src');
          setFirstCardSvg(srcAttribute);

          const keyAttribute =
            e.target.parentElement.parentElement.parentElement.getAttribute(
              'data-id'
            );
          setCurrentCardKey(keyAttribute);
        }
      }

      if (renderCount.current === 2) {
        if (e.target.parentElement.tagName === 'DIV') {
          e.target.parentElement.classList.add('card-is-flipped');

          e.target.parentElement.classList.add('card-two');

          const imgTag = e.target.parentElement.querySelector('.back-img');
          const srcAttribute = imgTag.getAttribute('src');
          const keyAttribute = e.target.parentElement.getAttribute('data-id');

          if (keyAttribute !== currentCardKey) {
            setSecondCardSvg(srcAttribute);
          } else {
          }
        } else if (e.target.parentElement.tagName === 'FIGURE') {
          e.target.parentElement.parentElement.classList.add('card-is-flipped');

          e.target.parentElement.parentElement.classList.add('card-two');

          const imgTag =
            e.target.parentElement.parentElement.querySelector('.back-img');
          const srcAttribute = imgTag.getAttribute('src');
          const keyAttribute =
            e.target.parentElement.parentElement.getAttribute('data-id');

          if (keyAttribute !== currentCardKey) {
            setSecondCardSvg(srcAttribute);
          } else {
          }
        } else if (e.target.parentElement.tagName === 'svg') {
          e.target.parentElement.parentElement.parentElement.classList.add(
            'card-is-flipped'
          );

          e.target.parentElement.parentElement.parentElement.classList.add(
            'card-two'
          );

          const imgTag =
            e.target.parentElement.parentElement.parentElement.querySelector(
              '.back-img'
            );
          const srcAttribute = imgTag.getAttribute('src');
          const keyAttribute =
            e.target.parentElement.parentElement.parentElement.getAttribute(
              'data-id'
            );

          if (keyAttribute !== currentCardKey) {
            setSecondCardSvg(srcAttribute);
          } else {
          }
        }
      }
    }
  };

  return (
    <>
      {svgData.map((singleSvg, index) => (
        <div
          key={singleSvg.uniqueId}
          data-id={singleSvg.uniqueId}
          className='card'
          onClick={clickHandler}
        >
          <CardFront />
          <CardBack singleSvg={singleSvg} />
        </div>
      ))}
    </>
  );
};

export default Card;

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGem } from '@fortawesome/free-regular-svg-icons';

const CardFront = () => {
  return (
    // <div className='card'>
    <figure className='front'>
      <FontAwesomeIcon className='gem-icon' icon={faGem} />
    </figure>
    // </div>
  );
};

export default CardFront;

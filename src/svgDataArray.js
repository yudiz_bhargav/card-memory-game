import svg1 from './svg/1.svg';
import svg2 from './svg/2.svg';
import svg3 from './svg/3.svg';
import svg4 from './svg/4.svg';
import svg5 from './svg/5.svg';
import svg6 from './svg/6.svg';
import svg7 from './svg/7.svg';
import svg8 from './svg/8.svg';
import svg9 from './svg/9.svg';
import svg10 from './svg/10.svg';
import svg11 from './svg/11.svg';
import svg12 from './svg/12.svg';
import svg13 from './svg/13.svg';
import svg14 from './svg/14.svg';
import svg15 from './svg/15.svg';

const svgData = [
  { uniqueId: 1, id: 1, svg1 },
  { uniqueId: 2, id: 2, svg2 },
  { uniqueId: 3, id: 3, svg3 },
  { uniqueId: 4, id: 4, svg4 },
  { uniqueId: 5, id: 5, svg5 },
  { uniqueId: 6, id: 6, svg6 },
  { uniqueId: 7, id: 7, svg7 },
  { uniqueId: 8, id: 8, svg8 },
  { uniqueId: 9, id: 9, svg9 },
  { uniqueId: 10, id: 10, svg10 },
  { uniqueId: 11, id: 11, svg11 },
  { uniqueId: 12, id: 12, svg12 },
  { uniqueId: 13, id: 13, svg13 },
  { uniqueId: 14, id: 14, svg14 },
  { uniqueId: 15, id: 15, svg15 },

  { uniqueId: 16, id: 1, svg1 },
  { uniqueId: 17, id: 2, svg2 },
  { uniqueId: 18, id: 3, svg3 },
  { uniqueId: 19, id: 4, svg4 },
  { uniqueId: 20, id: 5, svg5 },
  { uniqueId: 21, id: 6, svg6 },
  { uniqueId: 22, id: 7, svg7 },
  { uniqueId: 23, id: 8, svg8 },
  { uniqueId: 24, id: 9, svg9 },
  { uniqueId: 25, id: 10, svg10 },
  { uniqueId: 26, id: 11, svg11 },
  { uniqueId: 27, id: 12, svg12 },
  { uniqueId: 28, id: 13, svg13 },
  { uniqueId: 29, id: 14, svg14 },
  { uniqueId: 30, id: 15, svg15 },
];

// ARRAY SHUFFLE FUNCTION
function shuffle(array) {
  let currentIndex = array.length,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
}

shuffle(svgData);

export default svgData;

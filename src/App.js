import './App.css';
import Card from './card';

function App() {
  return (
    <div className='wrapper'>
      <Card />
    </div>
  );
}

export default App;
